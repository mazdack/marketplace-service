package com.example.marketplace;

import static java.util.Collections.emptyList;

import com.example.marketplace.entity.AccountType;
import com.example.marketplace.entity.PartnerAccount;
import com.example.marketplace.entity.ServiceEntity;
import com.example.marketplace.entity.ServiceSubscription;
import com.example.marketplace.entity.User;
import com.example.marketplace.repository.PartnerAccountRepository;
import com.example.marketplace.repository.ServiceRepository;
import com.example.marketplace.repository.ServiceSubscriptionRepository;
import com.example.marketplace.repository.UserRepository;

import java.util.stream.Stream;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
public class DemoApplication {

  @Bean
  public CommandLineRunner commandLineRunner(UserRepository userRepository,
                                             PartnerAccountRepository partnerAccountRepository,
                                             ServiceRepository serviceRepository,
                                             ServiceSubscriptionRepository serviceSubscriptionRepository,
                                             PasswordEncoder encoder) {
    return (args) -> {
      User consumer = userRepository.save(
        new User(null, "consumer", encoder.encode("123"), true, emptyList())
      );
      PartnerAccount consumerPartnerAccount = partnerAccountRepository.save(
        new PartnerAccount(
          null,
          consumer,
          AccountType.SERVICE_CONSUMER,
          "consumer's partner account", emptyList()
        )
      );

      Stream.of("Mary", "lev", "Simon", "Frank").forEach(username -> {
        User user = userRepository.save(
          new User(null, username, encoder.encode("123"), true, emptyList())
        );
        PartnerAccount partnerAccount = partnerAccountRepository.save(
          new PartnerAccount(
            null,
            user,
            AccountType.SERVICE_PROVIDER,
            username + "'s partner account",
            emptyList()
          )
        );
        ServiceEntity serviceEntity = serviceRepository.save(
          new ServiceEntity(
            null,
            username + "'s first service",
            "http://localhost:8081/",
            "description",
            partnerAccount
          )
        );

        serviceSubscriptionRepository.save(new ServiceSubscription(null, serviceEntity, consumerPartnerAccount, null));

      });

    };
  }

  public static void main(String[] args) {
    SpringApplication.run(DemoApplication.class, args);
  }
}


