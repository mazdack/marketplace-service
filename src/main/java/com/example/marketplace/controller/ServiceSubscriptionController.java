package com.example.marketplace.controller;

import static java.lang.String.format;

import com.example.marketplace.entity.AccountType;
import com.example.marketplace.entity.MyUserPrincipal;
import com.example.marketplace.entity.PartnerAccount;
import com.example.marketplace.entity.ServiceEntity;
import com.example.marketplace.entity.ServiceSubscription;
import com.example.marketplace.repository.PartnerAccountRepository;
import com.example.marketplace.repository.ServiceRepository;
import com.example.marketplace.repository.ServiceSubscriptionRepository;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/serviceSubscriptions")
public class ServiceSubscriptionController {
  private final PartnerAccountRepository partnerAccountRepository;
  private final ServiceRepository serviceRepository;
  private final ServiceSubscriptionRepository serviceSubscriptionRepository;

  public ServiceSubscriptionController(PartnerAccountRepository partnerAccountRepository,
                                       ServiceRepository serviceRepository,
                                       ServiceSubscriptionRepository serviceSubscriptionRepository) {
    this.partnerAccountRepository = partnerAccountRepository;
    this.serviceRepository = serviceRepository;
    this.serviceSubscriptionRepository = serviceSubscriptionRepository;
  }

  @PostMapping
  public ResponseEntity<Void> subscribe(@RequestParam("serviceId") Long serviceId,
                                        @RequestParam("partnerAccountId") Long partnerAccountId,
                                        @AuthenticationPrincipal MyUserPrincipal principal) {
    PartnerAccount partnerAccount = partnerAccountRepository.findById(partnerAccountId)
      .filter(pa -> pa.getUser().getId().equals(principal.getUserId()))
      .orElseThrow(() -> new ResponseStatusException(
        HttpStatus.NOT_FOUND,
        format("Partner account with id = %s is not found for user %s", partnerAccountId, principal.getUsername())
      ));

    if (partnerAccount.getAccountType() != AccountType.SERVICE_CONSUMER) {
      throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Only service consumer can subscribe to service");
    }

    ServiceEntity serviceEntity = serviceRepository.findById(serviceId)
      .orElseThrow(() -> new ResponseStatusException(
        HttpStatus.NOT_FOUND,
        format("Service wit id %s is not found", serviceId)
      ));

    serviceSubscriptionRepository.save(new ServiceSubscription(null, serviceEntity, partnerAccount, null));
    return ResponseEntity.status(HttpStatus.CREATED).build();
  }
}
