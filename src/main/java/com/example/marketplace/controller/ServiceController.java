package com.example.marketplace.controller;

import static java.lang.String.format;

import com.example.marketplace.entity.AccountType;
import com.example.marketplace.entity.MyUserPrincipal;
import com.example.marketplace.entity.PartnerAccount;
import com.example.marketplace.entity.ServiceEntity;
import com.example.marketplace.repository.PartnerAccountRepository;
import com.example.marketplace.repository.ServiceRepository;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/services")
public class ServiceController {
  private final PartnerAccountRepository partnerAccountRepository;
  private final ServiceRepository serviceRepository;

  public ServiceController(PartnerAccountRepository partnerAccountRepository, ServiceRepository serviceRepository) {
    this.partnerAccountRepository = partnerAccountRepository;
    this.serviceRepository = serviceRepository;
  }

  @PostMapping
  public ResponseEntity<Void> createService(@RequestParam("partnerAccountId") Long partnerAccountId,
                                              @RequestParam("name") String name,
                                              @RequestParam("providerUrl") String providerUrl,
                                              @RequestParam("apiDescription") String apiDescription,
                                              @AuthenticationPrincipal MyUserPrincipal principal) {
    PartnerAccount partnerAccount = partnerAccountRepository.findById(partnerAccountId)
      .filter(pa -> pa.getUser().getId().equals(principal.getUserId()))
      .orElseThrow(() -> new ResponseStatusException(
        HttpStatus.NOT_FOUND,
        format("Partner account with id = %s is not found for user %s", partnerAccountId, principal.getUsername())
      ));

    if (partnerAccount.getAccountType() != AccountType.SERVICE_PROVIDER) {
      throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Only service provider can create services");
    }
    serviceRepository.save(new ServiceEntity(null, name, providerUrl, apiDescription, partnerAccount));
    return ResponseEntity.status(HttpStatus.CREATED).build();
  }
}
