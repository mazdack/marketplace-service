package com.example.marketplace.controller;

import static java.lang.String.format;

import com.example.marketplace.entity.MyUserPrincipal;
import com.example.marketplace.entity.ServiceSubscription;
import com.example.marketplace.repository.ServiceSubscriptionRepository;

import javax.servlet.http.HttpServletRequest;

import org.springframework.cloud.gateway.mvc.ProxyExchange;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/gateway")
public class GatewayController {
  private final ServiceSubscriptionRepository serviceSubscriptionRepository;

  public GatewayController(ServiceSubscriptionRepository serviceSubscriptionRepository) {
    this.serviceSubscriptionRepository = serviceSubscriptionRepository;
  }

  @RequestMapping(path = "/{id}/{path}")
  public ResponseEntity<?> proxy(ProxyExchange<byte[]> proxy,
                                 @PathVariable("id") Long id,
                                 @PathVariable(value = "path", required = false) String path,
                                 @AuthenticationPrincipal MyUserPrincipal principal,
                                 HttpServletRequest request) {
    ServiceSubscription serviceSubscription = serviceSubscriptionRepository.findById(id)
      .filter(ss -> ss.getConsumerPartnerAccount().getUser().getId().equals(principal.getUserId()))
      .orElseThrow(() -> new ResponseStatusException(
        HttpStatus.NOT_FOUND,
        format("Subscription with id = %s is not found for user %s", id, principal.getUsername())
      ));

    ResponseEntity<byte[]> responseEntity;
    String providerUrlWithPath = serviceSubscription.getServiceEntity().getProviderUrl() + path;
    if (request.getMethod().equals("GET")) {
      responseEntity = proxy.uri(providerUrlWithPath).get();
    } else {
      responseEntity = proxy.uri(providerUrlWithPath).post();
    }

    return responseEntity;
  }

}
