package com.example.marketplace.entity;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PartnerAccount {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn
  private User user;
  @Column(nullable = false)
  @Enumerated(EnumType.STRING)
  private AccountType accountType;
  @Column(nullable = false)
  private String accountName;
  @OneToMany(mappedBy = "partnerAccount", cascade = CascadeType.ALL)
  private List<ServiceEntity> serviceEntities;

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    PartnerAccount partnerAccount = (PartnerAccount) o;

    return id.equals(partnerAccount.id);
  }

  @Override
  public int hashCode() {
    return id.hashCode();
  }
}
