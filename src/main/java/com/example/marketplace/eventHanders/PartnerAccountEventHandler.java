package com.example.marketplace.eventHanders;

import com.example.marketplace.entity.MyUserPrincipal;
import com.example.marketplace.entity.PartnerAccount;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
@RepositoryEventHandler
public class PartnerAccountEventHandler {
  @HandleBeforeCreate
  public void addUserToPartnerAccount(PartnerAccount partnerAccount) {
    MyUserPrincipal principal = (MyUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    partnerAccount.setUser(principal.getUser());
  }
}
