package com.example.marketplace.eventHanders;

import com.example.marketplace.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
@RepositoryEventHandler
public class UserEventHandler {

  @Autowired
  private PasswordEncoder passwordEncoder;

  @HandleBeforeCreate
  public void handleUserCreate(User user) {
    user.setPassword(passwordEncoder.encode(user.getPassword()));
  }
}
