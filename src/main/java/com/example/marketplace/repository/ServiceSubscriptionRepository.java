package com.example.marketplace.repository;

import com.example.marketplace.entity.ServiceSubscription;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

@RepositoryRestResource
public interface ServiceSubscriptionRepository extends CrudRepository<ServiceSubscription, Long> {

  @RestResource(path = "/my")
  @Query("select ss from ServiceSubscription ss where ss.consumerPartnerAccount.user.id = :#{ principal?.userId }")
  List<ServiceSubscription> findMyServices();
}
