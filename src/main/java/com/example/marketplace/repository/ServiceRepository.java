package com.example.marketplace.repository;

import com.example.marketplace.entity.ServiceEntity;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

@RepositoryRestResource(path = "/services")
public interface ServiceRepository extends CrudRepository<ServiceEntity, Long> {
  ServiceEntity save(ServiceEntity serviceEntity);

  @RestResource
  List<ServiceEntity> findAll();

  @RestResource(path = "/my")
  @Query("select service from ServiceEntity service where service.partnerAccount.user.id = :#{ principal?.userId }")
  List<ServiceEntity> findMyServices();
}
