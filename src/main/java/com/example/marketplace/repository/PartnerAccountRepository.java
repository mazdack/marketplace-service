package com.example.marketplace.repository;

import com.example.marketplace.entity.PartnerAccount;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

@Repository
public interface PartnerAccountRepository extends CrudRepository<PartnerAccount, Long> {
  @RestResource
  PartnerAccount save(PartnerAccount partnerAccount);

  @RestResource
  @Query("select partnerAccount from PartnerAccount partnerAccount where partnerAccount.user.id = :#{ principal?.userId }")
  List<PartnerAccount> findAll();
}
