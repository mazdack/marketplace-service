package com.example.marketplace.repository;

import com.example.marketplace.entity.User;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

@RepositoryRestResource
public interface UserRepository extends CrudRepository<User, Long> {

  @RestResource
  List<User> findAll();

  @RestResource
  User save(User user);

  User findByUsername(String username);
}
