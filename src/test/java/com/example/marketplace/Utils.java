package com.example.marketplace;

import com.example.marketplace.entity.AccountType;
import com.example.marketplace.entity.PartnerAccount;
import com.example.marketplace.entity.User;
import com.example.marketplace.repository.PartnerAccountRepository;
import com.example.marketplace.repository.UserRepository;
import java.util.Collections;
import java.util.List;
import javax.transaction.Transactional;

public class Utils {
  private final UserRepository userRepository;
  private final PartnerAccountRepository partnerAccountRepository;

  public Utils(UserRepository userRepository, PartnerAccountRepository partnerAccountRepository) {
    this.userRepository = userRepository;
    this.partnerAccountRepository = partnerAccountRepository;
  }

  @Transactional
  public User createUserWithPartnerAccount(String username, AccountType accountType) {
    User user = new User();
    user.setEnabled(true);
    user.setUsername(username);
    user.setPassword("password");

    PartnerAccount partnerAccount = new PartnerAccount(null, user, accountType, "description", Collections.emptyList());
    partnerAccount = partnerAccountRepository.save(partnerAccount);
    user.setPartnerAccounts(List.of(partnerAccount));

    userRepository.save(user);

    return user;
  }

  @Transactional
  public void clearUsers() {
    userRepository.deleteAll();
  }
}
