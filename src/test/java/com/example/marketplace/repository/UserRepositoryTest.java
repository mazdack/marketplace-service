package com.example.marketplace.repository;

import com.example.marketplace.TestConfiguration;
import com.example.marketplace.Utils;
import com.example.marketplace.entity.AccountType;
import com.example.marketplace.entity.User;
import javax.transaction.Transactional;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Import(TestConfiguration.class)
public class UserRepositoryTest {
  @Autowired
  private UserRepository userRepository;
  @Autowired
  private Utils utils;

  @After
  public void tearDown() {
    utils.clearUsers();
  }

  @Test
  @Transactional
  public void testUserToPartnerAccountLink() {
    User user = utils.createUserWithPartnerAccount("test", AccountType.SERVICE_CONSUMER);
    assertEquals(1, userRepository.findByUsername("test").getPartnerAccounts().size());
  }
}
