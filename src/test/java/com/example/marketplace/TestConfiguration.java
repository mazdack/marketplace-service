package com.example.marketplace;

import com.example.marketplace.repository.PartnerAccountRepository;
import com.example.marketplace.repository.UserRepository;
import org.springframework.context.annotation.Bean;

@org.springframework.boot.test.context.TestConfiguration
public class TestConfiguration {
  @Bean
  public Utils utils(UserRepository userRepository, PartnerAccountRepository partnerAccountRepository) {
    return new Utils(userRepository, partnerAccountRepository);
  }
}
