# Simple marketplace

- *To provide easy testing some entities are created by [CommandLineRunner](DemoApplication.commandLineRunner())* 
- [HAL browser](http://localhost:8080/browser/index.html#)

## Useful endpoints

### Create user
`http POST :8080/users username=lev3 password=123 enabled=true`

### Create partner account
` http -a lev3:123 POST :8080/partnerAccounts accountType=SERVICE_PROVIDER accountName=my`

### Create service
`http -f POST :8080/services name=test providerUrl=url apiDescription=desc partnerAccountId=22 -a lev4:123`
*service can be created only for SERVICE_PROVIDER partnerAccount*

### Create service subscription
`http -a lev:123 -f POST :8080/service-subscriptions serviceId=6 partnerAccountId=5`
*service subscription can be created only for SERVICE_CONSUMER partnerAccount *

### Work with service
` http :8080/gateway/{serviceSubscriptionId}/test test=123 -a consumer:123`
*Have a look for [echo-service](https://gitlab.com/mazdack/echo-service)*

## I wish i had more time or what can be done better
- replace basic authentication with something more useful for rest. Oauth2 for example
- replace gateway controller with separate gateway service on reactive stack. Can implement event sourcing for gateway info here.
- use hystrix for breaking circuit breaker and eureka for service discovery
- migrate from in-memory database to smth persistent
- add Spring Cloud Contract instead of plain text api description
- add unit tests (got jacoco test coverage monitoring already)
- add frontend of course :) 
